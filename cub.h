/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lejulien <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/01 22:16:24 by lejulien          #+#    #+#             */
/*   Updated: 2020/01/05 02:12:04 by lejulien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB_H
# define CUB_H
# include <fcntl.h>
# include <stdlib.h>
# include <stddef.h>
# include <unistd.h>

typedef struct          data_s
{
        void                    *mlx_ptr;
        void                    *mlx_win;
}                                       data_t;

typedef struct          player_s
{
        int                             x;
        int                             y;
        int                             o;
}                                       player_t;

typedef struct          pos_s
{
        int                             x;
        int                             y;
}                                       pos_t;

typedef struct          square_s
{
        pos_t                   pos;
        int                             w;
        int                             h;
}                                       square_t;

typedef struct	sort_s
{
	int		issave;
	char	*northpath;
	char	*southpath;
	char	*eastpath;
	char	*westpath;
	int		resw;
	int		resh;
	char	*sprite;
	int		rgbf;
	int		rgbc;
	int		mapwidth;
	int		mapheight;
}				sort_t;

void		ft_mlx_draw_square(square_t *square, data_t *data, int color);
void		ft_mlx_drawfilled_square(square_t *square, data_t *data, int color);
square_t	ft_set_square(int w, int h, int x, int y);
int			rgb_int(int red, int green, int blue);
#endif

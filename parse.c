/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lejulien <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/01 22:06:24 by lejulien          #+#    #+#             */
/*   Updated: 2020/01/03 06:15:30 by lejulien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include "cub.h"
#include "get_next_line.h"
#include "./libft-42/libft.h"

static sort_t
	ft_initialaze_sort(void)
{
	sort_t	sort;

	sort.issave = 0;
	sort.northpath = NULL;
	sort.southpath = NULL;
	sort.eastpath = NULL;
	sort.westpath = NULL;
	sort.resw = 600;
	sort.resh = 600;
	sort.sprite = NULL;
	sort.rgbf = 0;
	sort.rgbc = 0;
	return (sort);
}

int     rgb_int(int red, int green, int blue)
{
        int     rgb;
        rgb = red;
        rgb = (rgb << 8) + green;
        rgb = (rgb << 8) + blue;
        return (rgb);
}
																				#include <stdio.h>

static void
	ft_debug_sort(sort_t *sort)
{
	printf("is_save = %d\n", sort->issave);
	printf("northpath = %s\n", sort->northpath);
	printf("southpath = %s\n", sort->southpath);
	printf("eastpath = %s\n", sort->eastpath);
	printf("westpath = %s\n", sort->westpath);
	printf("resw = %d\n", sort->resw);
	printf("resh = %d\n", sort->resh);
	printf("sprite = %s\n", sort->sprite);
	printf("rgbf = %d\n", sort->rgbf);
	printf("rgbc = %d\n",  sort->rgbc);
	write(1, "map = \\/\n", 9);
}

void
	ft_put_error(void)
{
	printf("error");
	exit(0);
}

int
	strlennspace(char *str)
{
	int	i;

	i = 0;
	while (*str)
	{
		if (*str != ' ')
			i++;
		str++;
	}
	return (i);
}

char
	*ft_subspace(char *str)
{
	char	*temp;
	int		i;
	int		j;

	i = 0;
	j = 0;
	temp = malloc((strlennspace(str) + 1) * sizeof(char));
	while (str[i] != '\0')
	{
		while (str[i] == ' ')
			i++;
		temp[j] = str[i];
		j++;
		i++;
	}
	temp[j] = '\0';
	return (temp);
}

char
	*ft_compressmap(int fd)
{
	int		ret;
	char	*currentline;
	char	*previousline;
	char	*tofree;
	char	*temp;


	ret = 1;
	previousline = NULL;
	while (ret == 1)
	{
		ret = get_next_line(fd, &currentline);
		if (ret == 0)
			return (previousline);
		if (currentline[0] != '1' && ret == 1)
		{
			write(1, "oki\n", 4);
			ft_put_error();
		}
		if (previousline)
		{
			tofree = ft_strdup(previousline);
			tofree = ft_strjoin(previousline, "~");
			temp = ft_strjoin(tofree, currentline);
			ft_subspace(temp);
			free(tofree);
			free(previousline);
			previousline = ft_subspace(temp);
			free(temp);
			free(currentline);
		}
		else
			previousline = currentline;
	}
	return (NULL);
}

int
	ft_sort_and_rend(int fd, char ***map)
{
	char	*compressedmap;
	compressedmap = ft_compressmap(fd);
	*map = ft_split(compressedmap, '~');
	free(compressedmap);
	return (1);
}

void
	ft_debugmap(char **map)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (map[i][j])
	{
		while (map[i][j])
		{
			write(1, &map[i][j], 1);
			j++;
		}
		j = 0;
		write(1, "\n", 1);
		i++;
	}
}

int		ft_render(data_t *data, sort_t *sort)
{
		if (!(data->mlx_ptr = mlx_init()))
	{
		return (-1);
	}
	write(1, &sort->resw, 3);
		if (!(data->mlx_win = mlx_new_window(data->mlx_ptr, sort->resw, (const int)sort->resh, "cub3d")))
			return (-1);
	return (0);
}

int main(int argc, const char *argv[])
{
	data_t	data;
	sort_t	sort;
	int		fd;
	int		error;
	char	**map;

	error = 0;
	sort = ft_initialaze_sort();
	if (argc == 2 || argc == 3)
	{
		if ((fd = open(argv[1], O_RDONLY)) == -1)
			error = 1;
		if (argc == 3 && ft_strncmp(argv[2], "-save", 6) == 0)
			sort.issave = 1;
		else if (argc == 3)
			error = 1;
		if (error)
		{
			write(1, "Error\nBad Argument\n", 19);
			return (0);
		}
		sort.southpath = "southpath";
		sort.northpath = "northpath";
		printf("\n________________#DEBUG *.cub_____________\n\n");
		printf("\n________________#DEBUG *.cub_____________\n\n");
		ft_debug_sort(&sort);
		ft_sort_and_rend(fd, &map);
		ft_debugmap(map);
		//start game loiop
		ft_render(&data, &sort);
		mlx_loop(data.mlx_ptr);
	}
	return (0);
}
